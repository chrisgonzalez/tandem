export let timerange = null;

const tzOffset = new Date().getTimezoneOffset() * 60 * 1000;
const ecOffset = 300 * 60 * 1000;

export const setTimeRange = (start, end) => {
    timerange = { start, end };
}

export const clearTimeRange = () => { timerange = null; }

// convert user local time to east coast time
export const date = (datestring) => {
    const localDate = new Date(datestring);
    const localMinutes = localDate.getMinutes();
    const offset = localDate.getTimezoneOffset();
    localDate.setMinutes(localMinutes - offset + 300);
    return localDate;
}

export const midnight = timestamp => {
    const midnightToday = new Date(timestamp);
    midnightToday.setHours(0);
    midnightToday.setMinutes(0);
    midnightToday.setSeconds(0);
    midnightToday.setMilliseconds(0);

    const oneday = 24 * 60 * 60 * 1000;
    const today = midnightToday.getTime() - tzOffset + ecOffset;
    const tomorrow = today + oneday;
    const yesterday = today - oneday;

    let localMidnight;

    if (timestamp >= tomorrow) {
        localMidnight = tomorrow;
    } else if (timestamp >= today) {
        localMidnight = today;
    } else {
        localMidnight = yesterday;
    }

    return localMidnight;
}

export const formatTime = ts => {
    const d = date(ts);
    const hours = (d.getHours() + 1) % 12 === 0 ? 12: (d.getHours() + 1) % 12;
    const minutes = d.getMinutes() < 10 ? `0${d.getMinutes()}` : d.getMinutes();
    const ampm = d.getHours() > 11 ? 'pm' : 'am';
    return `${hours}:${minutes}${ampm}`;
}
