import { select, selectAll } from 'd3-selection';
import { min, max, median, mean } from 'd3-array';
import { scaleLinear } from 'd3-scale';
import { transition } from 'd3-transition';
import { line } from 'd3-shape';
import throttle from 'lodash/throttle';
import uniqBy from 'lodash/uniqBy';
import groupBy from 'lodash/groupBy';
import toArray from 'lodash/toArray';
import findIndex from 'lodash/findIndex';

import {
    timerange,
    setTimeRange,
    clearTimeRange,
    date,
    midnight,
    formatTime
} from './time';

let bgl, bolus;

let currentTrigger = -1;

const dpr = window.devicePixelRatio;

const scrollTriggers = [
    // Intro
    () => {
        console.log('intro');
        clearTimeRange();
        drawBGL([]);
        drawBGLLine();
        drawBolus([]);
        drawIOB([]);
    },
    // Orientation
    () => {
        console.log('orientation');
        clearTimeRange();
        drawBGL([]);
        drawBGLLine();
        drawBolus([]);
        drawIOB([]);
    },
    // In the beginning
    () => {
        console.log('show the scatterplot');
        clearTimeRange();
        drawBGL([]);
        drawBGLCanvas(bgl);
        drawBGLLine();
        drawBolus([]);
        drawIOB([]);
    },
    // I looked at months.
    () => {
        console.log('show a month');
        setTimeRange('3/1/2015', '4/1/2015');
        drawBGL([]);
        drawBGLCanvas(bgl);
        drawBGLLine(bgl);
        drawBolus([]);
        drawIOB([]);
    },
    // Days start with highs
    () => {
        console.log('show a day with a high start');
        setTimeRange('3/7/2015', '3/8/2015');
        drawBGL(bgl, false);
        drawBGLLine(bgl);
        drawBolus([]);
        drawIOB([]);
    },
    // Days start with lows
    () => {
        console.log('show a day with a low start');
        setTimeRange('2/19/2015', '2/20/2015');
        drawBGL(bgl, false);
        drawBGLLine(bgl);
        drawBolus([]);
        drawIOB([]);
    },
    // I plotted every day over many months to see how it looked.
    () => {
        console.log('zoom out, stack bgl');
        clearTimeRange();
        drawBGLLine();
        drawBGL([]);
        drawBolus([]);
        drawIOB([]);
    },
    // I used median values and times to give shape to my metabolism.
    () => {
        console.log('show median line');
        clearTimeRange();
        drawBGLLine(findMedianLine(bgl), true);
        drawBGL([]);
        drawBolus([]);
        drawIOB([]);
    },
    // Initial bolus
    () => {
        console.log('show bolus');
        setTimeRange('5/2/2015', '5/3/2015');
        drawBGL(bgl, false);
        drawBGLLine(bgl);
        drawBolus(bolus);
        drawIOB([]);
    },
    // IOB
    () => {
        console.log('show iob');
        setTimeRange('5/12/2015', '5/13/2015');
        drawBGL(bgl, false);
        drawBGLLine(bgl);
        drawBolus(bolus);
        drawIOB(bolus);
    },
    () => {
        clearTimeRange();
        drawBGL([]);
        drawBGLLine();
        drawBolus([]);
        drawIOB([]);
    },
    () => {
        clearTimeRange();
        drawBGL([]);
        drawBGLLine();
        drawBolus([]);
        drawIOB([]);
    }
]

const onScroll = () => {
    const sections = document.querySelectorAll('section');
    const viewportHeight = window.innerHeight;

    for (let i = 0, len = sections.length; i < len; i++) {
        const { top, bottom, height } = sections[i].getBoundingClientRect() || {};

        if ((top + viewportHeight / 3 > 0) && (top + viewportHeight / 3 < viewportHeight)) {
            if (i !== currentTrigger) {
                currentTrigger = i;
                document.querySelector('body').setAttribute('data-section', sections[i].getAttribute('data-section'));
                scrollTriggers[i]();
            }
        }
    }
}

const onResize = () => {
    const sections = document.querySelectorAll('section');
    const viewportHeight = window.innerHeight;

    for (let i = 0, len = sections.length; i < len; i++) {
        const { top, bottom, height } = sections[i].getBoundingClientRect() || {};

        if ((top + viewportHeight / 3 > 0) && (top + viewportHeight / 3 < viewportHeight)) {
            currentTrigger = i;
            document.querySelector('body').setAttribute('data-section', sections[i].getAttribute('data-section'));
            scrollTriggers[i]();
        }
    }


}

const initializeTriggers = () => {
    let supportsPassive = false;
    try {
      const opts = Object.defineProperty({}, 'passive', {
        get: function() {
          supportsPassive = true;
        }
      });
      window.addEventListener("test", null, opts);
    } catch (e) {}

    onScroll();

    window.addEventListener('scroll', throttle(onScroll, 150), supportsPassive ? { passive: true } : false);

    window.onresize = onResize;

    document.querySelector('.narrative').addEventListener('click', function(e) {
        const isButton = e.target.classList.contains('extra-info-button');

        if (isButton) {
            const extraInfo = e.target.nextElementSibling;
            extraInfo && extraInfo.classList.toggle('open');
        }
    });
}

/*
 * Zeroes out the timestamp to # milliseconds from midnight.
 */
const byTimeOfDay = data => {
    const flattenTimestamp = t => {
        const zeroPoint = midnight(t);
        return t - zeroPoint;
    };

    return data.map(d => (
        {
            timestamp: flattenTimestamp(d.timestamp),
            value: d.value
        }
    ));
}

// ********************************************************************** SCALES AND UTILS

const drawTimeline = (minDate, maxDate) => {
    const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'];
    const span = maxDate - minDate;
    const days = 24 * 60 * 60 * 1000;
    const range = (span > 31 * days && 'months') ||
                  (span > 1 * days && 'month') ||
                  'day';

    const startDate = date(minDate);
    const month = months[startDate.getMonth()];
    const day = startDate.getDate();

    const rangeLabel = (range === 'months' && ' ') ||
                       (range === 'month' && `${month} 2015`) ||
                       (range === 'day' && minDate && `${month} ${day}, 2015` ) ||
                       'Six Months';

    const ticks = (range === 'months' && ['Jan 2015', 'Mar 2015', 'May 2015']) ||
                  (range === 'month' && [8, 16, 24, 30]) ||
                  [6, 12, 18]

    const outOf = (range === 'month' && 31) ||
                  (range === 'day' && 24) ||
                  ticks.length;

    select('.range-label').text(rangeLabel);

    const tick = select('.timeline').selectAll('.tick').data(ticks);

    tick.enter()
        .append('div')
        .attr('class', 'tick')
        .merge(tick)
        .attr('data-label', d => {
            const ampm = d > 11 ? 'pm' : 'am';
            const hours = (d % 12 == 0 && 12 || d % 12);
            return (range === 'day' && `${hours}${ampm}`) ||
                   (range === 'month' && `${startDate.getMonth() + 1}/${d}`) ||
                   d;
        })
        .style('left', (d, i) => outOf !== ticks.length && `${100 * d / outOf}%` || `${100 * i / ticks.length}%`)

    tick.exit().remove();
}

const byTimeRange = data => (start, end) => {
    const startTs = date(start).getTime();
    const endTs = date(end).getTime();
    const results = data.filter(d => d.timestamp > startTs && d.timestamp < endTs);
    const oneBefore = [data[findIndex(data, d => d.timestamp === results[0].timestamp) - 1]] || [];
    const oneAfter = [data[findIndex(data, d => d.timestamp === results[results.length - 1].timestamp) + 1]] || [];

    return results.length && oneBefore.concat(results).concat(oneAfter) || [];
}

const bglScales = (data, forceRange) => {
    const { width, height } = select('.container').node().getBoundingClientRect();

    const minDate = (data && timerange && date(timerange.start).getTime()) ||
                    (data && !forceRange && min(data.map(d => d.timestamp))) ||
                    (data && forceRange && forceRange.start) ||
                    0;
    const maxDate = (data && timerange && date(timerange.end).getTime()) ||
                    (data && !forceRange && max(data.map(d => d.timestamp))) ||
                    (data && forceRange && forceRange.end) ||
                    0;

    const heightMax = width > 500 ? height / 4 : height / 3;

    const x = scaleLinear().domain([minDate, maxDate]).range([0, width]);
    const y = scaleLinear().domain([20, 500]).range([height * .98, heightMax]);
    const color = scaleLinear()
        .domain([30, 70, 170, 260])
        .range(["#ff0000", "#009b26","#006599", "#b70c3a"]);

    return { minDate, maxDate, x, y, color };
}

const bolusScales = data => {
    const { width, height } = select('.container').node().getBoundingClientRect();

    const minDate = (data && timerange && date(timerange.start).getTime()) ||
                    (data && min(data.map(d => d.timestamp))) ||
                    0;
    const maxDate = (data && timerange && date(timerange.end).getTime()) ||
                    (data && max(data.map(d => d.timestamp))) ||
                    0;
    const x = scaleLinear().domain([minDate, maxDate]).range([0, width]);
    const y = scaleLinear().domain([0, 20]).range([0, height]);
    const svgY = scaleLinear().domain([0, 20]).range([height, 0]);

    return { minDate, maxDate, x, y, svgY };
}

const findMedianLine = data => {
    const medianByHours = byTimeOfDay(data).reduce((p, c) => {
        const bglByHours = [].concat(p);
        const hour = Math.floor(c.timestamp / (60 * 60 * 1000));

        if (bglByHours[hour]) {
            bglByHours[hour].push(c);
        } else {
            bglByHours[hour] = [c];
        }

        return bglByHours;
    }, [])

    return medianByHours.filter(d => d)
        .map(hour => (
            {
                timestamp: median(hour, d => d.timestamp),
                value: hour.length >= 10 ? median(hour, d => d.value) : median(hour.concat(new Array(10 - hour.length).fill(mean(data, d => d.value))))
            }
        ));
}

// ********************************************************************** DRAW THINGS

const drawBGL = (data, bindByTimestamp = true) => {
    const values = timerange && byTimeRange(data || [])(timerange.start, timerange.end) || data;

    const bgl = select('.container').selectAll('.bgl').data(values, (d, i) =>  bindByTimestamp && d.timestamp || i);

    const { width, height } = select('.container').node().getBoundingClientRect();

    const { minDate, maxDate, x, y, color } = bglScales(values);

    drawTimeline(minDate, maxDate);

    const numberOfDaysInData = (maxDate - minDate) / (24 * 60 * 60 * 1000);

    const size = (numberOfDaysInData < 2 && 'large' ) ||
                 (numberOfDaysInData < 32 && 'medium' ) ||
                 'small';

    bgl.attr('class', () => `bgl bgl-${size}`)
        .style('transform', d => `translate3d(${x(d.timestamp)}px, ${y(d.value)}px, 0px)`)
        .attr('data-time', d => formatTime(d.timestamp))
        .attr('data-value', d => d.value)
        .style('border-color', d => color(d.value))
        .style('color', d => color(d.value))

    bgl.enter()
        .append('div')
        .each(function() {
            select(this).append('span');
        })
        .attr('class', () => `bgl bgl-${size}`)
        .attr('data-time', d => formatTime(d.timestamp))
        .attr('data-value', d => d.value)
        .style('transform', d => `translate3d(${x(d.timestamp)}px, ${y(d.value)}px, 0px)`)
        // .style('opacity', 0)
        .style('border-color', d => color(d.value))
        .style('color', d => color(d.value))
        // .transition()
        // .duration(300)
        // .style('opacity', 1)

    bgl.exit()
        .remove();

    // scale BGL range indicator

    const minBGL = 80;
    const maxBGL = 160;

    select('.bgl-range')
        .attr('data-min', minBGL)
        .attr('data-max', maxBGL)
        .style('height', `${y(minBGL) - y(maxBGL)}px`)
        .style('top', `${y(maxBGL)}px`);
}

const zeroTimestampsToDay = data => (
    data.map(d => (
        {
            timestamp: d.timestamp - midnight(d.timestamp),
            value: d.value
        }
    ))
)

const drawBGLCanvas = data => {
    const canvas = document.querySelector('.bgl-canvas');
    const context = canvas.getContext('2d');
    const { width, height } = select('.container').node().getBoundingClientRect();

    canvas.setAttribute('width', width * dpr);
    canvas.setAttribute('height', height * dpr);
    context.clearRect(0, 0, width * dpr, height * dpr);

    const { minDate, maxDate, x, y, color } = bglScales(data);

    data.forEach(function(d) {
        context.beginPath();
        context.arc(x(d.timestamp) * dpr, y(d.value) * dpr, 2 * dpr, 0, 2 * Math.PI, false);
        context.fillStyle = color(d.value);
        context.fill();
    })
}

const drawBGLLine = (data, forceRange) => {
    const values = timerange && byTimeRange(data || [])(timerange.start, timerange.end) || data || [];

    // console.log('drawing', values.length, 'to bgl line');

    const bglLine = select('.svg-container').selectAll('.bgl-line').data(values && [values] || [], d => d.length);

    const { minDate, maxDate, x, y } = bglScales(values, forceRange && { start: 0, end: 24 * 60 * 60 * 1000 } || null);

    const linegenerator = line().x(d => x(d.timestamp)).y(d => y(d.value));

    bglLine.transition()
        .duration(600)
        .attr('d', linegenerator)
        .style('opacity', 1);

    bglLine.enter()
        .append('path')
        .attr('class', 'bgl-line')
        .attr('d', linegenerator)
        .style('opacity', 0)
        .transition()
        .duration(600)
        .delay(150)
        .style('opacity', 1);

    bglLine.exit()
        .transition()
        .duration(100)
        .style('opacity', 0)
        .remove('path');
}

const drawBGLLines = data => {
    const canvas = document.querySelector('.bgl-lines');
    const context = canvas.getContext('2d');
    const { width, height } = select('.container').node().getBoundingClientRect();

    canvas.setAttribute('width', width * dpr);
    canvas.setAttribute('height', height * dpr);

    let bgldays = groupBy(data, function (item) {
        return midnight(item.timestamp);
    });

    bgldays = Object.keys(bgldays).map((midnightHour, i) => {
        return bgldays[midnightHour].map(d => (
            {
                timestamp: d.timestamp - midnightHour,
                value: d.value
            }
        ));
    });

    // console.log('drawing', bgldays.length, 'bgl lines');

    const { minDate, maxDate, x, y } = bglScales(bgldays.reduce((p, c) => p.concat(c), []), { start: 0, end: 24 * 60 * 60 * 1000 });

    bgldays.forEach(function(day) {
        for (let i = 0; i < day.length; i++) {
            const xPos = x(day[i].timestamp) * dpr;
            const yPos = y(day[i].value) * dpr;

            if(i == 0){
                context.moveTo(0, 0);
                context.beginPath();
                context.lineTo(xPos, yPos);
            }else{
                context.lineTo(xPos, yPos);
            }

            if(i == day.length - 1){
                context.lineWidth = 1;
                context.strokeStyle = "rgba(0,0,0,.2)";
                context.stroke();
            }
        }
    })
}


// ********************************************************************** BOLUS STUFF

const drawBolus = data => {
    const values = timerange && byTimeRange(data || [])(timerange.start, timerange.end) || data;

    // console.log('drawing', values.length, 'bolus');

    const { x, y } = bolusScales(values);

    const boluses = select('.container').selectAll('.bolus').data(values, d => d.timestamp);

    boluses
        .attr('data-value', d => `${d.value}u`)
        .style('left', d => `${x(d.timestamp)}px`)
        .transition()
        .duration(500)
        .style('height', d => `${y(d.value)}px`);

    boluses.enter()
        .append('div')
        .attr('class', 'bolus')
        .attr('data-value', d => `${d.value}u`)
        .style('left', d => `${x(d.timestamp)}px`)
        .style('height', d => `0px`)
        .style('opacity', 0)
        .transition()
        .duration(500)
        .delay((d, i) => 100 + 50 * i)
        .style('height', d => `${y(d.value)}px`)
        .style('opacity', 1);

    boluses.exit()
        .transition()
        .duration(500)
        .style('height', d => `0px`)
        .style('opacity', 0)
        .remove();
}

const drawIOB = data => {
    const values = timerange && byTimeRange(data || [])(timerange.start, timerange.end) || data;

    const hours = 60 * 60 * 1000;
    const bolusDuration = 4.5 * hours;
    const bolusPeak = .75 * hours;

    const doseRemaining = (bolus, timestamp) => {
        const isRampingUp = timestamp - bolus.timestamp < bolusPeak;
        const rampUpPercent = ((timestamp - bolus.timestamp) / bolusPeak);
        const rampDownPercent = (1 - (timestamp - (bolus.timestamp + bolusPeak)) / (bolusDuration - bolusPeak));

        return isRampingUp ? rampUpPercent * bolus.value : rampDownPercent * bolus.value;
    };

    const insulinOnBoard = (bolusTimestamp, timestamp) => (
        values.filter(bolus => timestamp <= bolus.timestamp + bolusDuration &&
                      timestamp  > bolus.timestamp &&
                      bolusTimestamp !== bolus.timestamp)
            .map(bolus => doseRemaining(bolus, timestamp))
            .reduce((total, currentDoseIOB) => (total + currentDoseIOB), 0)
    );


    // at ramp up, height of bolus = (bolus * bolusEnd) / (2 * bolusEnd - (bolusEnd - bolusPeak))
    const expandedBolus = values.map(d => {
        const { timestamp, value } = d;

        return [
            {
                timestamp: timestamp,
                value: insulinOnBoard(timestamp, timestamp)
            },
            {
                timestamp: timestamp + bolusPeak,
                value: value + insulinOnBoard(timestamp, timestamp + bolusPeak)
            },
            {
                timestamp: timestamp + bolusDuration,
                value: insulinOnBoard(timestamp, timestamp + bolusDuration)
            }
        ]
    }).reduce((p, c) => (
        p.concat(c)
    ), []).sort((a, b) => (
        (a.timestamp < b.timestamp && -1) ||
        (a.timestamp > b.timestamp && 1) ||
        0
    ));

    // console.log('drawing', expandedBolus.length, 'to iob');

    const { x, svgY } = bolusScales(expandedBolus);

    const linegenerator = line().x(d => x(d.timestamp)).y(d => svgY(d.value));

    const iob = select('.svg-container').selectAll('.iob').data([expandedBolus], d => d.length);

    iob.attr('d', linegenerator)
        .style('opacity', 1);

    iob.enter()
        .append('path')
        .attr('class', 'iob')
        .attr('d', linegenerator)
        .style('opacity', 0)
        .transition()
        .duration(500)
        .delay(100)
        .style('opacity', 1);

    iob.exit()
        .transition()
        .duration(500)
        .style('opacity', 0)
        .remove();
}

/*
 * Fetch some data!
 */
fetch('data/bgl-2016.json')
    .then((response, data) => {
        response.json().then(json => {
            bgl = json;
            fetch('data/bolus-2016.json')
                .then((response, data) => {
                    response.json().then(json => {
                        bolus = json;
                        drawBGLLines(bgl);
                        initializeTriggers();
                    })
                });
            })
    });


console.log('%c Hello console people! Enjoy the demo ;)', 'background: #121212; color: #08A5B9; font-family: sans-serif; font-size: 16px; height: 50px;');
console.log('%c To see the code, check out', 'background: #121212; color: #08a5b9; font-family: sans-serif; font-size: 16px; height: 50px;');
console.log('%c http://github.com/chrisgonzalez/tandem', 'background: #121212; color: #fefefe; font-family: sans-serif; font-size: 16px; height: 50px;');
