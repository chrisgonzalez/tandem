# Tandem Diabetes Demo

Provides a walkthrough of my diabetes data explorations over the past five years.

## Orientation

This project uses a scaffolding for simple, single-page prototypes written in ES6 and bundled simply via npm scripts. This particular project uses `d3.js` and a tiny bit of `lodash`.

* All data sources are located in `/data`
* All application code is located in `/js`
* All styles are located in `/css/styles.clean.css`
* All writeup and document structure is in `index.html`

## To run

I use `nvm` locally, but any version of `Node >= 6.2.2` should work!

To run locally:

* `nvm use;`
* `npm install;`
* `npm start;`

The app will be served at `http://localhost:3000`

To build:

* `npm run build`
